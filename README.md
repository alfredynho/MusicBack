## Vuejs Backend

**Vuejs** Proyect Django - Vuejs
### Tecnologías

  * [Django](https://www.djangoproject.com/)
  * [Admin LTE Template](https://adminlte.io/themes/AdminLTE/index2.html)

### Commercial Support

# Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE website_app"`
  - `psql -c "DROP USER website_user"`
  - `psql -c "CREATE USER website_user WITH NOCREATEDB NOCREATEUSER ENCRYPTED PASSWORD 'XXXYYYZZZ'"`
  - `psql -c "CREATE DATABASE website_app WITH OWNER website_user"`

DEVZONE es soportado por [@alfredynho](alfredynho.cg@gmail.com).